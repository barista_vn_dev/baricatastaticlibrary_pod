# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Installation with CocoaPods ###
* To integrate BaricataStaticLibrary into your Xcode project using CocoaPods, specify it in your Podfile:

        pod 'BaricataStaticLibrary', :git => 'https://bitbucket.org/barista_vn_dev/baricatastaticlibrary_pod.git', :tag => '1.0.6'

* Then, run the following command:

        $ pod install
        
### Use with ObjectC ###
* Import header

        #import <BaricataStaticLibrary/BaricataStaticLibrary.h>
        
* Show Baricata

        [BaricataStaticLibrary openBaricataWithRootController:self];
        
### Use with Swift ###
* Add Bridging Header
        In Build Settings, in Swift Compiler - Code Generation, make sure the Objective-C Bridging Header build setting has a path to the Baricata bridging header file 
        [Screenshot](https://drive.google.com/open?id=0B5ptHm5ABXzjMmpoSWNpWGhZVm8)
        
        ${PODS_ROOT}/BaricataStaticLibrary/BaricataStaticLibrary-Bridging-Header.h
 
* Show Baricata

        BaricataStaticLibrary.openBaricata(withRootController: self)

### LICENSE ###

* Barista.co.jp
