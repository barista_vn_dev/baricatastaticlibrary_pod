//
//  BaricataStaticLibrary.h
//  BaricataStaticLibrary
//
//  Created by barista-is8@barista.co.jp on 6/28/17.
//  Copyright © 2017 barista. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaricataStaticLibrary : NSObject

+ (void)openBaricataWithRootController:(UIViewController *)rootController;

@end
